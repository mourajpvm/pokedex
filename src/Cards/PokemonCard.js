

import React from 'react';
import cx from 'clsx';
import { makeStyles } from '@mui/styles';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import TextInfoContent from '@mui-treasury/components/content/textInfo';
import { useFourThreeCardMediaStyles } from '@mui-treasury/styles/cardMedia/fourThree';
import { useN04TextInfoContentStyles } from '@mui-treasury/styles/textInfoContent/n04';
import { useOverShadowStyles } from '@mui-treasury/styles/shadow/over';

import './PokemonCard.css'

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 343,
    margin: 'auto',
    borderRadius: 12,
    padding: 12,
  },
  media: {
    borderRadius: 6,
  },
}));

export const MusicCardDemo = React.memo(function MusicCard(props) {
  const styles = useStyles();
  const mediaStyles = useFourThreeCardMediaStyles();
  const textCardContentStyles = useN04TextInfoContentStyles();
  const shadowStyles = useOverShadowStyles({ inactive: true });
  return (
    <Card
      className={cx(styles.root, shadowStyles.root)}
      sx={{
        width: 250,
        maxWidth: "100%",
        borderRadius: "12px",
        padding: 1.5,
        margin: 1,
        backgroundColor: "#f5f5f5",
        boxShadow: "2px 14px 80px rgba(34, 35, 58, 0.2)",
      }}
    >
      <CardMedia
        className={cx(styles.media, mediaStyles.root)}
        image={props.image}
        sx={{
          borderRadius: "6px",
          width: "auto",
          height: "auto",
          paddingBottom: "min(75%, 240px)",
          backgroundColor: "#f9f1dc" ,
        }}
      />
      <CardContent>
        <TextInfoContent
          classes={textCardContentStyles}
          overline={props.type}
          heading={props.name.toUpperCase()}
          body={
            <td className="Table">
              <tr>Number: #{props.number}</tr>
              <tr>Species: {props.species}</tr>
              <tr>Hp: {props.hp}</tr>
              <tr>Attack: {props.attack}</tr>
              <tr>Defense: {props.defense}</tr>
              <tr>Speed: {props.speed}</tr>
            </td>
          }
        />
      </CardContent>
    </Card>
  );
});
export default MusicCardDemo