import "./App.css";
import { useState } from "react";
import Axios from "axios";
import PokemonCard from "./Cards/PokemonCard";



const App = () => {

  const [pokemonFamily, setPokemonFamily] = useState({
    family: []
  });

  const [pokemonName, setPokemonName] = useState("");
  const [pokemonChosen, setPokemonChosen] = useState(false);
  const [pokemon, setPokemon] = useState({
    name: "",
    number: "",
    species: "",
    image: "",
    hp: "",
    attack: "",
    defense: "",
    speed: "",
    type: ""
  });

  const searchPokemon = () => {
    Axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`).then(
      (res) => {
        setPokemon({
          name: pokemonName,
          number: res.data.id,
          species: res.data.species.name,
          image: res.data.sprites.front_default,
          hp: res.data.stats[0].base_stat,
          attack: res.data.stats[1].base_stat,
          defense: res.data.stats[2].base_stat,
          speed: res.data.stats[5].base_stat,
          type: res.data.types[0].type
        });
        setPokemonChosen(true);
      }
    );
  };


  const searchType = () => {
      Axios.get(`${pokemon.type.url}`).then(
        (res) => {
          setPokemonFamily({
            family: res.data.pokemon
          });
          console.log( res.data.pokemon);
          
          pokemonFamily.family = res.data.pokemon; 
        }
      );
  };

  function RenderFamily(family) {
       return (
      <ol>
        {family.map((pk) => (
          <li key={pk.pokemon.name}>{pk.pokemon.name}
           
          </li>
        ))}
      </ol>
    );
  }

  function RenderPokemon(pokemon, family) {

        return (
          <>
            <div>
              {pokemon.type.url && (
                <button className="ButtonSearch" onClick={searchType}>
                  Search Pokémon with same Type
                </button>
              )}
            </div>

            <PokemonCard
              number={pokemon.number}
              name={pokemon.name}
              image={pokemon.image}
              species={pokemon.species}
              type={pokemon.type.name}
              hp={pokemon.hp}
              attack={pokemon.attack}
              defense={pokemon.defense}
              speed={pokemon.speed}
            />

            <div className="SecondSection" onClick={searchType}>
              <h5>{RenderFamily(family)}</h5>
            </div>
          </>
        );
}

  return (
    <div className="App">
      <div className="TitleSection">
        <h2>Pokédex</h2>
        <input
          type="text"
          onChange={(event) => {
            setPokemonName(event.target.value);
          }}
          value={pokemonName.toLowerCase()}
        />
        <div>
          {pokemonName && (
            <button onClick={searchPokemon}>Search Pokémon</button>
          )}
        </div>
      </div>
      <div className="DisplaySection">
        {!pokemonChosen ? (
          <h1>Please choose a Pokémon</h1>
        ) : (
          <>{RenderPokemon(pokemon, pokemonFamily.family)}</>
        )}
      </div>
    </div>
  );
};

export default App;